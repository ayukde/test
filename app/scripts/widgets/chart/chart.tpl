<div class="chart"></div>
<div class="chart-options">
    <div class="chart-option chart-type chart-type-line" data-type="line">Line</div>
    <div class="chart-option chart-type chart-type-column" data-type="column">Column</div>
    <div class="chart-option chart-type chart-type-pie" data-type="pie">Pie</div>
    <!--<div class="chart-option expand-chart">Expand Chart</div>-->
	
	<label class="chart-option" for="modal">Expand Chart</label>
	<div class="modal-box">
	  <input type="checkbox" class="toggle-on" id="modal">
	  <label class="toggle-off" for="modal"></label>
	  <div class="content modal-inner">
	    <span>
	    <div class="chart">must show chart here...</div>
	    <div class="chart-options">
		    <div class="chart-option chart-type chart-type-line" data-type="line">Line</div>
		    <div class="chart-option chart-type chart-type-column" data-type="column">Column</div>
		    <div class="chart-option chart-type chart-type-pie" data-type="pie">Pie</div>
		</div>

	    <label class="toggle-button" for="modal">Close</label>
	    </span>
	  </div>
	</div>

</div>