/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager'
],
function (Backbone, LayoutManager) {
    return Backbone.Model.extend({
        url: function () {
            return '/data/chart' + this.id + '.json';
        },
        defaults: {
            chartName: '',
            chartType: 'column',
            series: [],
            xAxis: []
        },
        updateChartSeries: function (newSeries, newCategories) {
            if (!this._isValidSeries(newSeries)) { throw new Error("Invalid Chart Series"); }
            if (!_.isEmpty(newCategories) && !_.isNull(newCategories)) { throw new Error("You must supply a category list to go with the new series"); }

            this.series = newSeries;
            // Then we trust the programmer knows what they're doing and
            // we don't have to update the categories
            if (!_.isNull(newCategories)) {
                this.categories = newCategories;
            }

            this.trigger('inform:seriesUpdated');
        },
        updateChartType: function (newChartType) {
            if (!this._isValidChartType(newChartType)) { throw new Error("Unknown Chart Type"); }

            this.chartType = newChartType;

            this.trigger('inform:chartTypeUpdated', newChartType);
        },
        _isValidSeries: function (series) {
            // If you like you could implement this method fully
            return !_.isEmpty(series);
        },
        _isValidChartType: function (chartType) {
            var validCharts = ["column", "line", "pie"];

            return _.contains(validCharts, chartType);
        }
    });
});