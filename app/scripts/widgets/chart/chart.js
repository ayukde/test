/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager',
    'highcharts',
    './model/chart',
    'text!./chart.tpl'
],
function (
    Backbone,
    LayoutManager,
    Highcharts,
    ChartModel,
    chartTemplate
) {
    return Backbone.Layout.extend({
        template: chartTemplate,
        events: {
            'click .chart-type': function (e) {
                var $element = $(e.target),
                    chartType = $element.data('type');

                if ($element.hasClass('disabled')) { return; }

                this.model.updateChartType(chartType);
            }
        },
        initialize: function (opts) {
            var that = this;

            this.model = new ChartModel({ id: opts.chartId });
            this.model.fetch().done(function () {
                that.render();
            }).fail(function () {
                console.log('nope');
            });
            this.chartInstance = null;
            this._initialiseListeners();
        },
        beforeRender: function () {
        },
        afterRender: function () {
            this.$('.chart').highcharts({
                chart: {
                    type: this.model.get('chartType')
                },
                xAxis: this.model.get('xAxis'),
                yAxis: this.model.get('yAxis'),
                series: this.model.get('series'),
                credits: { enabled: false },
                title: {
                    text: this.model.get('chartName')
                }
            });
            this.chartInstance = this.$('.chart').highcharts();
            this._checkChartTypes();
        },
        _checkChartTypes: function () {
            var chartType = this.model.get('chartType');

            if (this.chartInstance.series.length > 1) {
                this.$('.chart-type-pie').addClass('disabled');
            }
        },
        _initialiseListeners: function () {
            this.model.on('inform:chartTypeUpdated', function (chartType) {
                // Just flow through as it's easier, if you want to please feel free to improve this section
                switch (chartType) {
                    case 'pie':
                        if (this.model.get('series').length > 1) {
                            throw new Error('warning: too many series for a pie chart type');
                            break;
                        }
                    case 'column':
                    case 'line':
                    default:
                        _.each(this.chartInstance.series, function (series) {
                            series.update({
                                type: chartType
                            }, false);
                        }, this);
                        this.chartInstance.redraw();
                        break;
                }
            }, this);
        }
    });
});