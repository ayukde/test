/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager',
    'text!./menuItem.tpl'
],
function (Backbone, LayoutManager, menuTemplate) {
    return Backbone.Layout.extend({
        template: menuTemplate,
        tagName: 'li',
        className: 'menu-item',
        events: {
            'click': function () {
                this.trigger('request:followTarget', this.model.get('target'));
            }
        },
        initialize: function () {
        },
        serialize: function () {
            return this.model.toJSON()
        },
        beforeRender: function () {
        },
        afterRender: function () {
        }
    });
});