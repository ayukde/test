/*global require*/
'use strict';

define([
    'backbone',
    'layoutmanager',
    './menuItem/menuItem',
    'App',
    'text!./menu.tpl'
],
function (Backbone, LayoutManager, MenuItemView, App, menuTemplate) {
    return Backbone.Layout.extend({
        template: menuTemplate,
        tagName: 'ul',
        className: 'menu',
        initialize: function () {
            this.menuCollection = new Backbone.Collection([{
                name: 'Home',
                target: '/'
            }, {
                name: 'Help',
                target: '/help'
            }]);
        },
        beforeRender: function () {
            _.each(this.menuCollection.models, function (item) {
                var menuItem = new MenuItemView({ model: item });
                menuItem.on('request:followTarget', function (target) {
                    App.router.navigate(target, {trigger: true});
                }, this);
                this.insertView(menuItem);
            }, this);
        },
        afterRender: function () {
        }
    });
});