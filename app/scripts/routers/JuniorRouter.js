/*global require*/
'use strict';

define([
    'backbone',
    'pages/index/index',
    'pages/chart/chart',
    'pages/help/help',
    'widgets/menu/menu'
],
function (
    Backbone,
    IndexView,
    ChartView,
    HelpView,
    MenuView
) {
    return Backbone.Router.extend({
        routes: {
            '':             'index',
            'chart/:id':    'chart',
            'help':         'help',
            '*default':     'default'
        },
        init: function () {
            var menuWidget = new MenuView();
            menuWidget.render();
            $('#menu').html(menuWidget.el);
        },
        index: function () {
            var indexPage = new IndexView();
            indexPage.render();
            $('#app-junior').html(indexPage.el);
        },
        chart: function (chartId) {
            var chartPage = new ChartView({ chartId: chartId });
            chartPage.render();
            $('#app-junior').html(chartPage.el);
        },
        help: function () {
            var helpPage = new HelpView();
            helpPage.render();
            $('#app-junior').html(helpPage.el);
        },
        default: function () { console.log('default'); }
    });
});